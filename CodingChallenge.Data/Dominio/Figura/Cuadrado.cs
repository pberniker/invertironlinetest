﻿namespace CodingChallenge.Data.Dominio.Figura
{
    public class Cuadrado : BaseForma, IFigura
    {
        public Cuadrado(decimal ancho) : base("Cuadrado", ancho)
        {
        }

        public decimal CalcularArea()
        {
            return Redondear(Ancho * Ancho);
        }

        public decimal CalcularPerimetro()
        {
            return Redondear(Ancho * 4);
        }
    }
}
