﻿namespace CodingChallenge.Data.Dominio.Figura
{
    public class Circulo : BaseForma, IFigura
    {
        public Circulo(decimal ancho) : base("Circulo", ancho)
        {
        }

        public decimal CalcularArea()
        {
            return Redondear(PI * (Ancho / 2) * (Ancho / 2));
        }

        public decimal CalcularPerimetro()
        {
            return Redondear(PI * Ancho);
        }
    }
}
