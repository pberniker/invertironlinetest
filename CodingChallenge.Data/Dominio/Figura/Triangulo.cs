﻿using System;

namespace CodingChallenge.Data.Dominio.Figura
{
    public class Triangulo : BaseForma, IFigura
    {
        public Triangulo(decimal ancho) : base("Triangulo", ancho)
        {
        }

        public decimal CalcularArea()
        {
            return Redondear((decimal) Math.Sqrt(3) / 4 * Ancho * Ancho);
        }

        public decimal CalcularPerimetro()
        {
            return Redondear(Ancho * 3);
        }
    }
}
