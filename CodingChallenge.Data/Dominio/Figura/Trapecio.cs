﻿namespace CodingChallenge.Data.Dominio.Figura
{
    public class Trapecio : BaseForma, IFigura
    {
        private decimal _baseMenor;
        private decimal _baseMayor;
        private decimal _ladoMenor;

        public Trapecio(decimal baseMenor, decimal baseMayor, decimal ladoMenor, decimal ladoMayor) : base("Trapecio", ladoMayor)
        {
            _baseMayor = baseMayor;
            _baseMenor = baseMenor;
            _ladoMenor = ladoMenor;
        }

        public decimal CalcularArea()
        {
            return Redondear((_baseMenor + _baseMayor) / 2 * _ladoMenor);
        }

        public decimal CalcularPerimetro()
        {
            return Redondear(_baseMenor + _baseMayor + _ladoMenor + Ancho);
        }
    }
}
