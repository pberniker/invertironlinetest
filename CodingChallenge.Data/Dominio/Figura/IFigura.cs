﻿namespace CodingChallenge.Data.Dominio.Figura
{
    public interface IFigura
    {
        decimal CalcularArea();
        decimal CalcularPerimetro();
    }
}
