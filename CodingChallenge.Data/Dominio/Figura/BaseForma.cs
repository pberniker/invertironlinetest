﻿using System;

namespace CodingChallenge.Data.Dominio.Figura
{
    public abstract class BaseForma
    {
        protected const decimal PI = (decimal) Math.PI;

        private string Nombre { get; set; }
        protected decimal Ancho { get; }

        protected BaseForma(string nombre, decimal ancho)
        {
            Nombre = nombre;
            Ancho = ancho;
        }

        public override string ToString()
        {
            return Nombre;
        }

        protected decimal Redondear(decimal valor)
        {
            return decimal.Round(valor,2,MidpointRounding.AwayFromZero);
        }
    }
}
