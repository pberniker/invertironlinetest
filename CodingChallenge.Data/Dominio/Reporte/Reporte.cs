﻿using System.Collections.Generic;

namespace CodingChallenge.Data.Dominio.Reporte
{
    public class Reporte
    {
        public Informe.Informe Informe{ get; set; }
        public Dictionary<string, string> Traducciones { get; set; }
    }
}
