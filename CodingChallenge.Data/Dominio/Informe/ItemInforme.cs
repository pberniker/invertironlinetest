﻿namespace CodingChallenge.Data.Classes.Informe
{
    public class ItemInforme
    {
        public string Figura { get; set; }
        public decimal Area { get; set; }
        public decimal Perimetro { get; set; }
    }
}
