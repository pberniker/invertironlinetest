﻿namespace CodingChallenge.Data.Classes.Informe
{
    public class SubTotalIforme
    {
        public int Cantidad { get; set; }
        public decimal Area { get; set; }
        public decimal Perimetro { get; set; }
    }
}
