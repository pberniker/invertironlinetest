﻿using System.Collections.Generic;
using CodingChallenge.Data.Classes.Informe;

namespace CodingChallenge.Data.Dominio.Informe
{
    public class Informe
    {
        public IEnumerable<ItemInforme> Items { get; set; }
        public Dictionary<string, SubTotalIforme> SubTotales { get; set; }
        public TotalIforme Totales { get; set; }
    }
}
