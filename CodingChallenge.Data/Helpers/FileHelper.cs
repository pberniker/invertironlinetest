﻿using System.IO;

namespace CodingChallenge.Data.Helpers
{
    public static class FileHelper
    {
        public static string Leer(string dir)
        {
            var res = "";
            var baseDir = Path.GetDirectoryName(typeof(FileHelper).Assembly.Location);
            var path = Path.Combine(baseDir, dir);

            using (var reader = new StreamReader(path))
            {
                res = reader.ReadToEnd();
                reader.Close();
            }

            return res;
        }
    }
}
