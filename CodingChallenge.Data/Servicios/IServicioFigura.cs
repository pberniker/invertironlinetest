﻿using CodingChallenge.Data.Classes.Informe;
using CodingChallenge.Data.Dominio.Informe;

namespace CodingChallenge.Data.Servicios
{
    public interface IServicioFigura
    {
        Informe Calcular();
    }
}
