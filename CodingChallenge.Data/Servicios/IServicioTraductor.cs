﻿namespace CodingChallenge.Data.Servicios
{
    public interface IServicioTraductor
    {
        string Traducir(string palabra);
    }
}
