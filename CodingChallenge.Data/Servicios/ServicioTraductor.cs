﻿using System;
using System.Collections.Generic;
using CodingChallenge.Data.Helpers;
using Newtonsoft.Json;

namespace CodingChallenge.Data.Servicios
{
    public class ServicioTraductor : IServicioTraductor
    {
        private readonly Dictionary<string, string> _traduccciones;

        public ServicioTraductor(string idioma)
        {
            _traduccciones = Cargar(idioma);
        }

        public string Traducir(string palabra)
        {
            try
            {
                return _traduccciones[palabra];
            }
            catch (Exception ex)
            {
                throw new Exception("Traduccion no encontrada");
            }
        }

        private Dictionary<string, string> Cargar(string idioma)
        {
            var json = FileHelper.Leer($"Resources\\{idioma}.json");
            var res = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            return res;
        }
    }
}
