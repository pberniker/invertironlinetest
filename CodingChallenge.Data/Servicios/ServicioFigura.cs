﻿using System.Collections.Generic;
using System.Linq;
using CodingChallenge.Data.Classes.Informe;
using CodingChallenge.Data.Dominio.Figura;
using CodingChallenge.Data.Dominio.Informe;

namespace CodingChallenge.Data.Servicios
{
    public class ServicioFigura : IServicioFigura
    {
        private readonly List<IFigura> _figuras;

        public ServicioFigura()
        {
            _figuras = new List<IFigura>();
        }

        public void Agregar(IFigura figura)
        {
            _figuras.Add(figura);
        }

        public Informe Calcular()
        {
            var items = new List<ItemInforme>();
            var subTotales = new Dictionary<string, SubTotalIforme>();
            var totales = new TotalIforme();

            if (_figuras.Any())
            {
                items = CalcularItems();
                subTotales = CalcularSubTotales(items);
                totales = CalcularTotales(subTotales);
            }

            var res = new Informe { Items = items, SubTotales = subTotales, Totales = totales };

            return res;
        }

        private List<ItemInforme> CalcularItems()
        {
            var res = new List<ItemInforme>();

            foreach (IFigura figura in _figuras)
            {
                var area = figura.CalcularArea();
                var perimetro = figura.CalcularPerimetro();
                var item = new ItemInforme { Figura = figura.ToString(), Area = area, Perimetro = perimetro };
                res.Add(item);
            }

            return res;
        }

        private static Dictionary<string, SubTotalIforme> CalcularSubTotales(IEnumerable<ItemInforme> items)
        {
            var res = new Dictionary<string, SubTotalIforme>();

            var itemsOrdenados = items.OrderBy(x => x.Figura).ToList();
            var cantidad = itemsOrdenados.Count;
            var j = 0;

            while (j < cantidad)
            {
                var itemAnterior = itemsOrdenados[j];
                var k = 0;
                var acumArea = 0m;
                var acumPerimetro = 0m;

                while (j < cantidad && itemAnterior.Figura == itemsOrdenados[j].Figura)
                {
                    k ++;
                    acumArea += itemsOrdenados[j].Area;
                    acumPerimetro += itemsOrdenados[j].Perimetro;
                    j++;
                }

                res.Add(
                    itemAnterior.Figura,
                    new SubTotalIforme
                        {
                            Cantidad = k, 
                            Area = acumArea,
                            Perimetro = acumPerimetro
                        }
                );
            }

            return res;
        }

        private static TotalIforme CalcularTotales(Dictionary<string, SubTotalIforme> subTotales)
        {
            var res = new TotalIforme
                { 
                    Cantidad = subTotales.Sum(x => x.Value.Cantidad),
                    Area = subTotales.Sum(x => x.Value.Area),
                    Perimetro = subTotales.Sum(x => x.Value.Perimetro)
                };

            return res;
        }
    }
}
