﻿using System.Collections.Generic;
using CodingChallenge.Data.Dominio.Reporte;
using CodingChallenge.Data.Helpers;
using RazorEngine;

namespace CodingChallenge.Data.Servicios
{
    public class ServicioReporte
    {
        private readonly IServicioFigura _servicioFigura;
        private readonly IServicioTraductor _servicioTraductor;

        public ServicioReporte(IServicioFigura servicioFigura, IServicioTraductor servicioTraductor)
        {
            _servicioFigura = servicioFigura;
            _servicioTraductor = servicioTraductor;
        }

        public string Obtener()
        {
            var informe = _servicioFigura.Calcular();

            if (informe.Totales == null || informe.Totales.Cantidad == 0)
            {
                var traduccion = _servicioTraductor.Traducir("ListaVacia");
                return $"<h1>{traduccion}!</h1>";
            }

            var content = FileHelper.Leer("Views\\Report.cshtml");

            var model = new Reporte { Informe = informe, Traducciones = ObtenerTraducciones() };
            var str = Razor.Parse(content, model);

            const string marca = "<!-- Comienzo -->";

            var i = str.IndexOf(marca);
            var res = i == -1? str : str.Substring(i + marca.Length); 

            return res;
        }

        private Dictionary<string, string> ObtenerTraducciones()
        {
            var res = new Dictionary<string, string>();

            AgregarTraduccion(res, "ReporteFormas");
            AgregarTraduccion(res, "Figura");

            AgregarTraduccion(res, "Cantidad");
            AgregarTraduccion(res, "Area");
            AgregarTraduccion(res, "Perimetro");

            AgregarTraduccion(res, "Circulo");
            AgregarTraduccion(res, "Cuadrado");
            AgregarTraduccion(res, "Trapecio");
            AgregarTraduccion(res, "Triangulo");

            AgregarTraduccion(res, "Subtotal");
            AgregarTraduccion(res, "Total");

            return res;
        }

        private void AgregarTraduccion(Dictionary<string, string> traductiones, string palabra)
        {
            traductiones.Add(palabra, _servicioTraductor.Traducir(palabra));
        }
    }
}
