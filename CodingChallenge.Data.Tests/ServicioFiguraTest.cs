﻿using System.Linq;
using CodingChallenge.Data.Dominio.Figura;
using CodingChallenge.Data.Servicios;
using NUnit.Framework;

namespace CodingChallenge.Data.Tests
{
    [TestFixture]
    public class ServicioFiguraTest
    {
        [TestCase]
        public void TestVacio()
        {
            // Arrange
            var servicioFigura = new ServicioFigura();

            // Act
            var informe = servicioFigura.Calcular();

            // Assert
            Assert.AreEqual(informe.Totales.Cantidad, 0);
        }

        [TestCase]
        public void TestConUnCuadrado()
        {
            // Arrange

            var servicioFigura = new ServicioFigura();
            servicioFigura.Agregar(new Cuadrado(5));

            // Act

            var informe = servicioFigura.Calcular();
            var subTotales = informe.SubTotales;
            var subTotal = subTotales["Cuadrado"];
            var totales = informe.Totales;

            // Assert

            Assert.AreEqual(subTotal.Area, 25);
            Assert.AreEqual(subTotal.Perimetro, 20);

            Assert.AreEqual(totales.Area, 25);
            Assert.AreEqual(totales.Perimetro, 20);

            Assert.AreEqual(totales.Cantidad, 1);
        }


        [TestCase]
        public void TestConMasCuadrados()
        {
            // Arrange

            var servicioFigura = new ServicioFigura();

            servicioFigura.Agregar(new Cuadrado(5));
            servicioFigura.Agregar(new Cuadrado(1));
            servicioFigura.Agregar(new Cuadrado(3));

            // Act

            var informe = servicioFigura.Calcular();
            var items = informe.Items;
            var item = items.ElementAt(1);
            var totales = informe.Totales;

            // Assert

            Assert.AreEqual(item.Area, 1);
            Assert.AreEqual(item.Perimetro, 4);

            Assert.AreEqual(totales.Area, 35);
            Assert.AreEqual(totales.Perimetro, 36);

            Assert.AreEqual(totales.Cantidad, 3);
        }

        [TestCase]
        public void TestConMasTipos()
        {
            // Arrange
            var servicioFigura = new ServicioFigura();

            servicioFigura.Agregar(new Cuadrado(5));
            servicioFigura.Agregar(new Circulo(3));
            servicioFigura.Agregar(new Triangulo(4));

            servicioFigura.Agregar(new Cuadrado(2));
            servicioFigura.Agregar(new Triangulo(9));
            servicioFigura.Agregar(new Circulo(2.75m));

            servicioFigura.Agregar(new Triangulo(4.2m));

            // Act

            var informe = servicioFigura.Calcular();

            var subTotales = informe.SubTotales;
            var subTotalesCuadrado = subTotales["Cuadrado"];
            var subTotalesCirculo = subTotales["Circulo"];
            var subTotalesTriangulo = subTotales["Triangulo"];

            var totales = informe.Totales;

            // Assert

            Assert.AreEqual(subTotalesCuadrado.Cantidad, 2);
            Assert.AreEqual(subTotalesCuadrado.Area, 29);
            Assert.AreEqual(subTotalesCuadrado.Perimetro, 28);

            Assert.AreEqual(subTotalesCirculo.Cantidad, 2);
            Assert.AreEqual(subTotalesCirculo.Area, 13.01);
            Assert.AreEqual(subTotalesCirculo.Perimetro, 18.06);

            Assert.AreEqual(subTotalesTriangulo.Cantidad, 3);
            Assert.AreEqual(subTotalesTriangulo.Area, 49.64);
            Assert.AreEqual(subTotalesTriangulo.Perimetro, 51.6);

            Assert.AreEqual(totales.Cantidad, 7);
            Assert.AreEqual(totales.Area, 91.65);
            Assert.AreEqual(totales.Perimetro, 97.66);
        }

        [TestCase]
        public void TestConnMasTrapecios()
        {
            // Arrange

            var servicioFigura = new ServicioFigura();

            servicioFigura.Agregar(new Trapecio(4, 5, 3, 3.2m));
            servicioFigura.Agregar(new Trapecio(10, 20, 5, 10.5m));

            // Act

            var informe = servicioFigura.Calcular();
            var items = informe.Items;
            var item = items.First();
            var totales = informe.Totales;

            // Assert

            Assert.AreEqual(item.Area, 13.5);
            Assert.AreEqual(item.Perimetro, 15.2);

            Assert.AreEqual(totales.Area, 88.5);
            Assert.AreEqual(totales.Perimetro, 60.7);

            Assert.AreEqual(totales.Cantidad, 2);
        }
    }
}
