﻿using System;
using CodingChallenge.Data.Servicios;
using NUnit.Framework;

namespace CodingChallenge.Data.Tests
{
    [TestFixture]
    public class ServicioTraductorTest
    {
        [TestCase]
        public void TraducirEspTest()
        {
            // Arrange
            var servicioTradcutor = new ServicioTraductor("esp");

            // Act
            var res = servicioTradcutor.Traducir("ListaVacia");

            // Assert
            Assert.AreEqual(res, "Lista vacia de formas");
        }

        [TestCase]
        public void TraducirIngTest()
        {
            // Arrange

            var servicioTradcutor = new ServicioTraductor("ing");

            // Act

            var res = servicioTradcutor.Traducir("ListaVacia");

            // Assert

            Assert.AreEqual(res, "Empty list of shapes");
        }


        [TestCase]
        public void TraducirExcepcion()
        {
            // Arrange

            var servicioTradcutor = new ServicioTraductor("esp");

            // Assert

            Assert.Throws<Exception>(() => servicioTradcutor.Traducir("Hola"));
        }
    }
}
