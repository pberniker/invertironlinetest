﻿using System.Collections.Generic;
using CodingChallenge.Data.Classes.Informe;
using CodingChallenge.Data.Dominio.Informe;
using CodingChallenge.Data.Servicios;
using Moq;
using NUnit.Framework;

namespace CodingChallenge.Data.Tests
{
    [TestFixture]
    public class ServicioReporteTest
    {
        [TestCase]
        public void TestSinDatos()
        {
            // Arrange

            var mockServicioFigura = new Mock<IServicioFigura>();
            mockServicioFigura.Setup(x => x.Calcular()).Returns(new Informe());

            var mokServicioTraductor = new Mock<IServicioTraductor>();
            mokServicioTraductor.Setup(x => x.Traducir("ListaVacia")).Returns("Lista vacia de formas");

            var servicioReporte = new ServicioReporte(mockServicioFigura.Object, mokServicioTraductor.Object);

            // Act

            var res = servicioReporte.Obtener();

            // Assert

            Assert.AreEqual(res, "<h1>Lista vacia de formas!</h1>");
        }

        [TestCase]
        public void TestConDatos()
        {
            // Arrange

            #region mockServicioFigura

            var informe = new Informe
                {
                    Items = new List<ItemInforme>
                        {
                            new ItemInforme { Figura = "Cuadrado", Area = 25m, Perimetro = 20m },
                            new ItemInforme { Figura = "Circulo", Area = 7.07m, Perimetro = 9.42m },
                            new ItemInforme { Figura = "Triangulo", Area = 6.93m, Perimetro = 12m },

                            new ItemInforme { Figura = "Cuadrado", Area = 4m, Perimetro = 8m },
                            new ItemInforme { Figura = "Circulo", Area = 35.07m, Perimetro = 27m },
                            new ItemInforme { Figura = "Triangulo", Area = 5.94m, Perimetro = 8.64m },

                            new ItemInforme { Figura = "Triangulo", Area = 7.64m, Perimetro = 12.6m }
                        },

                    SubTotales = new Dictionary<string, SubTotalIforme>
                        {
                            { "Circulo", new SubTotalIforme { Cantidad = 2, Area = 29m, Perimetro = 28m } },
                            { "Cuadrado", new SubTotalIforme { Cantidad = 2, Area = 13.01m, Perimetro = 18.06m } },
                            { "Triangulo", new SubTotalIforme { Cantidad = 3, Area = 49.64m, Perimetro = 51.6m } }
                        },

                    Totales = new TotalIforme
                        {
                            Cantidad = 7,
                            Area = 91.65m,
                            Perimetro = 97.66m
                        }
                };

            var mockServicioFigura = new Mock<IServicioFigura>();
            mockServicioFigura.Setup(x => x.Calcular()).Returns(informe);

            #endregion

            #region mokServicioTraductor

            var mokServicioTraductor = new Mock<IServicioTraductor>();

            mokServicioTraductor.Setup(x => x.Traducir("ReporteFormas")).Returns("Reporte de formas");
            mokServicioTraductor.Setup(x => x.Traducir("Figura")).Returns("Figura");

            mokServicioTraductor.Setup(x => x.Traducir("Cantidad")).Returns("Cantidad");
            mokServicioTraductor.Setup(x => x.Traducir("Area")).Returns("Area");
            mokServicioTraductor.Setup(x => x.Traducir("Perimetro")).Returns("Perimetro");

            mokServicioTraductor.Setup(x => x.Traducir("Circulo")).Returns("Circulo");
            mokServicioTraductor.Setup(x => x.Traducir("Cuadrado")).Returns("Cuadrado");
            mokServicioTraductor.Setup(x => x.Traducir("Trapecio")).Returns("Trapecio");
            mokServicioTraductor.Setup(x => x.Traducir("Triangulo")).Returns("Triangulo");

            mokServicioTraductor.Setup(x => x.Traducir("Subtotal")).Returns("Subtotal");
            mokServicioTraductor.Setup(x => x.Traducir("Total")).Returns("Total");

            #endregion

            #region resEsperada
            const string resEsperada = @"
                <h1>Reportedeformas</h1>
                <table>
                    <thead>
                        <tr>
                            <th>Figura</th>
                            <th>Cantidad</th>
                            <th>Area</th>
                            <th>Perimetro</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Cuadrado</td>
                            <td>1</td>
                            <td>25</td>
                            <td>20</td>
                        </tr>
                        <tr>
                            <td>Circulo</td>
                            <td>1</td>
                            <td>7,07</td>
                            <td>9,42</td>
                        </tr>
                        <tr>
                            <td>Triangulo</td>
                            <td>1</td>
                            <td>6,93</td>
                            <td>12</td>
                        </tr>
                        <tr>
                            <td>Cuadrado</td>
                            <td>1</td>
                            <td>4</td>
                            <td>8</td>
                        </tr>
                        <tr>
                            <td>Circulo</td>
                            <td>1</td>
                            <td>35,07</td>
                            <td>27</td>
                        </tr>
                        <tr>
                            <td>Triangulo</td>
                            <td>1</td>
                            <td>5,94</td>
                            <td>8,64</td>
                        </tr>
                        <tr>
                            <td>Triangulo</td>
                            <td>1</td>
                            <td>7,64</td>
                            <td>12,6</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan=""4"">Subtotal</th>
                        </tr>
                        <tr>
                            <td>Circulo</td>
                            <td>2</td>
                            <td>29</td>
                            <td>28</td>
                        </tr>
                        <tr>
                            <td>Cuadrado</td>
                            <td>2</td>
                            <td>13,01</td>
                            <td>18,06</td>
                        </tr>
                        <tr>
                            <td>Triangulo</td>
                            <td>3</td>
                            <td>49,64</td>
                            <td>51,6</td>
                        </tr>
                        <tr>
                            <th colspan=""4"">Total</th>
                        </tr>
                        <tr>
                            <td></td>
                            <th>7</th>
                            <th>91,65</th>
                            <th>97,66</th>
                        </tr>
                    </tfoot>
                </table>
            ";
            #endregion

            var servicioReporte = new ServicioReporte(mockServicioFigura.Object, mokServicioTraductor.Object);

            // Act

            var res = servicioReporte.Obtener();

            // Assert

            Assert.AreEqual(LimpiarTexto(res),LimpiarTexto(resEsperada));
        }

        private static string LimpiarTexto(string str)
        {
            var res = str.Trim().TrimStart().TrimEnd().Replace("\r", "").Replace("\n", "").Replace(" ", "");
            return res;
        }
    }
}
